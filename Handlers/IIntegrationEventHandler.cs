namespace Core.EventBus.Handlers;

using System.Threading.Tasks;

using Events;

public interface IIntegrationEventHandler<in T> : IIntegrationEventHandler
    where T : IntegrationEvent
{
    Task Handle(T @event);
}

public interface IIntegrationEventHandler
{

}
