pipeline {
    agent {
        docker {
            image 'mcr.microsoft.com/dotnet/sdk:6.0-alpine'
        }        
    }
    environment {
        DOTNET_CLI_HOME = "/tmp/DOTNET_CLI_HOME"
        IGNORE_NORMALISATION_GIT_HEAD_MOVE = 1
        NEXUS_NUGET_API_KEY = credentials('nexus-nuget-api-key')
        NEXUS_USERNAME = credentials('nexus-username')
        NEXUS_PASSWORD = credentials('nexus-password')
    }
    stages {
        stage('Build') {
            steps {
                sh 'dotnet nuget disable source nuget.org'
                sh 'dotnet nuget add source $NEXUS_NUGET_GROUP -n nuget-group -u $NEXUS_USERNAME -p $NEXUS_PASSWORD --store-password-in-clear-text'
                sh 'dotnet clean'
                sh 'dotnet restore'
                sh 'dotnet build --configuration Release'
            }
        }
        stage('Publish') {
            steps {
                sh 'dotnet tool install --global gitversion.tool'
                sh '$DOTNET_CLI_HOME/.dotnet/tools/dotnet-gitversion /output buildserver'
                script {
                    def branchName = "master";
                    def buildNumber = "0";
                    def semVersion = "0.1.0";
                    
                    def filePath = readFile "${WORKSPACE}/gitversion.properties";
                    def lines = filePath.readLines();
                    
                    for (line in lines) {
                        def (name, value) = line.tokenize('=');
                        switch(name) {
                            case "GitVersion_BranchName":
                                branchName = value;
                                break;
                            case "GitVersion_BuildMetaData":
                                buildNumber = value;
                                break;
                            case "GitVersion_SemVer":
                                semVersion = value;
                                break;
                        }
                    }
                    
                    if (branchName.contains("feature")) {
                        def (feature, code) = branchName.tokenize('/');
                        env.SEMANTIC_VERSION = buildNumber != null ? "${semVersion}-alpha-${code}-${buildNumber}" : "${semVersion}-alpha-${code}-0";
                        return;
                    }
                    
                    if (buildNumber != null) {
                        env.SEMANTIC_VERSION = "${semVersion}-preview-${buildNumber}";
                        return;
                    }
                    
                    env.SEMANTIC_VERSION = "${semVersion}";
                }
                sh 'dotnet pack --configuration Release /p:Version=$SEMANTIC_VERSION'
                sh 'dotnet nuget push ./bin/Release/*.nupkg -k $NEXUS_NUGET_API_KEY -s $NEXUS_NUGET_HOSTED'
                sh 'rm -rf ./bin'
                sh 'rm -rf ./obj'
            }
        }
    }
}